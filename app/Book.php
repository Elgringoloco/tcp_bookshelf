<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    // This allows us to use the fields in the CREATE / UPDATE operations.
    protected $fillable = ['title', 'author', 'isbn', 'api_response', 'cover_id', 'user_id', 'at'];
    

    protected $casts = [
        'api_response' => 'array',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
