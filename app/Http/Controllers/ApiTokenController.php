<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ApiTokenController extends Controller
{
     /**
     * Update the authenticated user's API token.
     * Displays once and then not again. 
     * 
     * A nice upgrade would be to set an expiration date and force the 
     * user to update again. This would prevent api token theft, but is beyond
     * the scope of this demo.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function update(Request $request)
    {
        $token = Str::random(60);

        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return ['token' => $token];
    }
}
