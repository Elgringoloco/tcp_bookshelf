<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $books = Auth::user()->books;
        return view('bookshelf')->with('books', $books);
    }

    public function store(Request $request)
    {
        $user_id = Auth::user()->id;

        $validated = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'author' => ['required', 'string', 'max:255'],
            'isbn' => ['required', 'string', 'max:255'],
            'cover_id' => ['string'],
            'api_response' => ['json'],
        ]);

        $validated['user_id'] = $user_id;
        $validated['at'] = Book::where('user_id', '=', $user_id)->max('at') + 1 ?? 1;
        $book = Book::create($validated);
        return response()->json($book, 201);
    }

    /**
     *  A naive implementation that would not be very effective on very large lists. 
     * It  potentially requires updating many rows depending on the new position of the item. 
     * 
     *  For further reading see the below post and the discussion on hacker news. 
     *  - https://begriffs.com/posts/2018-03-20-user-defined-order.html
     *  - https://news.ycombinator.com/item?id=16635440
     */
    public function updateOrder(Request $request, Book $book)
    {
        $user_id = Auth::user()->id;
        $request->validate([
            'at' => ['required', 'integer', 'gt:0'],
        ]);
        $position = $request->at;
        $prev_position = $book->at;

        $max = Book::where([
            ['user_id', '=', $user_id],
        ])->max('at');

        //  Cases in which we need to return early.
        if ($position == $prev_position) {
            // Order is the same that it is already  so just return the books.
            $books = Book::where('user_id', $user_id)->get();
            return response()->json($books, 200);
        } else if ($position > $max) {
            // UI should not allow this but we can never trust the client to behave.
            $message = array("message" => "Invalid order position");
            return response()->json($message, 400);
        }
        
        
        if ($position < $prev_position) {
            Book::where([
                ['user_id', '=', $user_id],
                ['at', '>=', $position],
                ['at', '<', $prev_position]
            ])->increment('at');
        } else if ($position > $prev_position) {
            Book::where([
                ['user_id', '=', $user_id],
                ['at', '<=', $position],
                ['at', '>', $prev_position]
            ])->decrement('at');
        }

        $book->at = $position;
        $book->save();
        $books = Book::where('user_id', $user_id)->get();
        return response()->json($books, 201);
    }

    public function delete(Request $request, Book $book) {
        $user_id = Auth::user()->id;
        Log::info($book);
        Log::info($user_id);
        if ($book->user_id != $user_id) {
            $message = array("message" => "Not Authorized to remove this resource");
            return response()->json($message, 403);
        } else {
            $position = $book->at;

            $book->delete();
            //  Update order of other books. 
            Book::where([
                ['user_id', '=', $user_id],
                ['at', '>', $position]
            ])->decrement('at');
            $message = array("message" => "Resource deleted");
            response()->json($message, 202);
        }

    }

    /**
     * Show the profile for the given book.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        return view('book.detail', ['book' => Book::findOrFail($id)]);
    }

}
