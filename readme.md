# TCP Bookshelf

The purpose of this project is to demonstrate working capacity in Laravel and VueJS.


![Example Video](docs/example.mp4)


## Booj Reading List
*Beware of the person of one book. -- Thomas Aquinas*
### Task
Compose a site using the [Laravel](https://laravel.com/) or Vue framework that allows the user to create a list of books they would like to read. Users should be able to perform the following actions:
* Connect to a publically available API
* Create Postman collection and Vue app OR Laravel App 
* Add or remove items from the list
* Change the order of the items in the list
* Sort the list of items
* Display a detail page with at least 3 points of data to display
* Include unit tests
* Deploy it on the cloud - be prepared to describe your process on deployment

### Tech Stack

* PHP 7.3
* Laravel 6.x
* NPM 
* NGINX
* Vue  


### Testing

Tests can be run with the following command

```
./vendor/bin/phpunit --testdox
```


### Deployment

* Project will be deployed on a DigitalOcean Server running `Ubuntu 16.04`
* Ensure the `.env` file is configured 
* Ensure appropriate read/write permissions are configured for the various directories on the server.

