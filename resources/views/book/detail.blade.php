@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           <h1>Book Details</h1>
           <a href="/bookshelf">Return to Bookshelf</a>
           <hr>
           <book-details :book='{{json_encode($book)}}'></book-details>
        </div>
    </div>
</div>
@endsection
