@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <book-shelf :shelf='{{json_encode($books)}}'></book-shelf>
        </div>
    </div>
</div>
@endsection
