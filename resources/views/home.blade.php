@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div style="margin-bottom: 1rem;" class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h2>Welcome to the bookshelf application</h2>
                    <p>
                    This is a demonstration of working in Laravel and Vue.
                    <ul>
                        <li>
                            Before you can add books to your bookshelf you must retrieve a token.
                        </li>
                        <li>
                            Clicking on 'Retrieve a New Token' will open a new window with a
                            JSON response including this token. It will only be displayed once
                            so ensure that you copy the value correctly.
                        </li>
                        <li>
                            Once you have the token, paste the value into the input field and
                            then press the button to persist the value to localStorage <em>(which is a bit of a security concern)</em>
                        </li>
                        <li>
                            If at anytime you click the 'Retrieve a New Token' anchor tag again, the token must be set again.
                        </li>
                    </ul>
                    </p>
                    <api-token></api-token>
                    <br>
                    <p>
                     Once your token has been set, navigate to your <a href="/bookshelf">bookshelf</a> and begin adding books.
                    </p>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
