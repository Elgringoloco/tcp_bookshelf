<?php

use Illuminate\Http\Request;
use App\Book;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('books', function() {
    return Book::all();
});

Route::get('books/{id}', function($id) {
    return Book::find($id);
});

Route::middleware('auth:api')->post('books', 'BookController@store');
Route::middleware('auth:api')->put('books/order/{book}', 'BookController@updateOrder');
Route::middleware('auth:api')->delete('books/{book}', 'BookController@delete');


// Route::put('books/{id}', function(Request $request, $id) {
//     $book = Book::findOrFail($id);
//     $book->update($request->all());
//     return $book;
// });


// List Books by User ID
// Route::get('/user/{id}/books', function (Request $request) {
//     return $request->user();
// });

Route::get('/book', function (Request $request) {
    return $request->book();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

