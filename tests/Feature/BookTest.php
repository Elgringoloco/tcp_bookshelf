<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class BookTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testShouldNotAllowUnauthenticatedUserToCreateResource()
    {

        $data = array(
            "title" => "Lord of The Rings",
        );
        $response = $this->json('POST', '/api/books', $data);

        $response->assertStatus(401);
    }

    public function testShouldAllowAuthenticatedUserToGetToken()
    {
        $user = factory(User::class)->make(); // Creates a user object

        $response = $this->actingAs($user)->json('GET', '/token');

        $response->assertStatus(200);
        $response->assertJson([
            "token" => true,
        ]);
    }

    public function testShouldAllowAuthenticatedUserWithTokenToCreateResource()
    {

        $user = factory(User::class)->make(); // Creates a user object but does not insert

        $response = $this->actingAs($user)->json('GET', '/token');

        $response->assertStatus(200);
        $response->assertJson([
            "token" => true,
        ]);

        $content = $response->getContent();
        $token = json_decode($content)->token;
        $header = 'Bearer ' . $token;


        $json = json_encode([
            "hello" => "world"
        ]);

        $response_2 = $this->actingAs($user)
            ->withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])
            ->json('POST', '/api/books', [
                "title" => "Lord of The Rings",
                "author" => 'J.R.R',
                "isbn" => "123",
                "cover_id" => "1",
                "api_response" => $json
            ]
            );

        $response_2->assertStatus(201);
    }

}
